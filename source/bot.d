module bot;

import db;
import run;
import speech;
import tg.d;

import std.range;
import std.algorithm;
import std.typecons;
import std.format;
import std.conv : to;

import vibe.core.core;
import vibe.core.log;

import vibe.data.json;

enum MAX_STDIN_LENGTH = 512;
enum MAX_ARGS_LENGTH  = 128;
enum MAX_CODE_LENGTH  = 2000;
enum MAX_OUTPUT_LENGTH = 1750;

struct Dlangbot {
	private {
		TelegramBot m_bot;
		DB m_db;
		Runner m_run;
	}

	this(string bottoken, string dburl, string runurl) {
		m_bot = TelegramBot(bottoken);
		m_db  = DB(dburl);
		m_run = Runner(runurl);

		auto me = m_bot.getMe;
		"This bot info:"     .logInfo;
		"\tID: %d"           .logInfo(me.id);
		"\tIs bot: %s"       .logInfo(me.is_bot);
		"\tFirst name: %s"   .logInfo(me.first_name);
		"\tLast name: %s"    .logInfo(me.last_name.isNull     ? "null" : me.last_name);
		"\tUsername: %s"     .logInfo(me.username.isNull      ? "null" : me.username);
		"\tLanguage code: %s".logInfo(me.language_code.isNull ? "null" : me.language_code);

		import core.time;
		1.seconds.setTimer(() =>
			this.handleUpdates(m_bot.updateGetter(0, ["message", "edited_message", "callback_query"])), true);
	}

	int run() { return runApplication(); }

	void handleUpdates(Range)(Range r)
	if(isInputRange!Range && is(ElementType!Range == Update)) {
		r.witch!(a => this.hasPendingAction(a.edited_message),
				 a => runTask(() => m_bot.sendMessageMarkdown(a.edited_message.chat.id, Speech!"cannot_edit_while_command")))
		 .witch!(a => this.hasPendingAction(a.message),
		 		(a) {
					if(this.isCommand(a.message)) {
						runTask(() => this.handlePendingActionCommand(a));
					} else {
						runTask(() => this.handlePendingAction(a));
					}
				})
		 .witch!(a => this.isCommand(a.edited_message),
				 a => runTask(() => m_bot.sendMessageMarkdown(a.edited_message.chat.id, Speech!"cannot_edit_command")))
		 .witch!(a => this.isCommand(a.message),
				 a => runTask(() => this.handleCommand(a)))
		 .witch!(a => !a.edited_message.isNull && !a.edited_message.text.isNull,
		 		 a => runTask(() => this.handleEditedMessage(a)))
		 .witch!(a => !a.message.isNull && !a.message.text.isNull && a.message.text.length >= "void main(){}".length,
		 		 a => runTask(() => this.handleMessage(a)))
		 .witch!(a => !a.callback_query.isNull,
				 a => runTask(() => this.handleCallback(a)))
		 .each!( a => runTask(() => this.handleOtherUpdates(a)));
	}

	void handleCommand(Update u)
	in {
		assert(!u.message.isNull);
		assert(!u.message.text.isNull);
		assert(!u.message.from.isNull);
	} body {
		"Handling command %s".logDebug(u.message.text);

		switch(u.message.text.until('@').to!string) {
		case "/start":
			runTask({ m_db.addChat(u.message.chat.id); });
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"start");
			return;
		case "/help" : m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"help");  return;
		case "/settings":
			auto s = m_db.getUserSettings(u.message.chat.id, u.message.from.id);
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"settings".format(s.compiler, s.stdin, s.args));
			return;
		case "/dmd":
			m_db.setUserSetting!"compiler"(u.message.chat.id, u.message.from.id, "dmd");
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"compiler_set".format("dmd"));
			return;
		case "/ldc":
			m_db.setUserSetting!"compiler"(u.message.chat.id, u.message.from.id, "ldc");
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"compiler_set".format("ldc"));
			return;
		case "/compiler":
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.changeCompilerSetting);
			m_bot.sendMessageForceReply(u.message.chat.id, Speech!"compiler_set_command");
			return;
		case "/stdin":
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.changeStdinSetting);
			m_bot.sendMessageForceReply(u.message.chat.id, Speech!"stdin_set_command");
			return;
		case "/args":
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.changeArgsSetting);
			m_bot.sendMessageForceReply(u.message.chat.id, Speech!"args_set_command");
			return;
		case "/execute":
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.executeCode);
			m_bot.sendMessageForceReply(u.message.chat.id, Speech!"execute_code");
			return;
		case "/cancel": goto case "/empty";
		case "/empty" :
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"nothing_to_cancel_or_empty".format(u.message.text));
			return;
		case "/statistics":
			auto s = m_db.getStatistics;
			m_bot.sendMessageMarkdown(u.message.chat.id,
				Speech!"show_statistics".format(
					s.chats,
					s.successful + s.failed,
					s.successful,
					s.failed,
					s.characters,
					s.output,
					s.errors,
					s.warnings));
			return;
		default:
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"unknown_command".format(u.message.text));
			return;
		}
	}

	void handlePendingActionCommand(Update u)
	in {
		assert(!u.message.isNull);
		assert(!u.message.text.isNull);
		assert(!u.message.from.isNull);
	} body {
		"Handling penging action command".logDebug;

		switch(u.message.text.until('@').to!string) {
		case "/empty":
			final switch(m_db.getUserAction(u.message.chat.id, u.message.from.id)) with(Action) {
			case none:
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"nothing_to_cancel_or_empty".format(u.message.text));
				break;
			case changeCompilerSetting:
				m_db.setUserSetting!"compiler"(u.message.chat.id, u.message.from.id, "dmd");
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"empty_compiler");
				break;
			case changeStdinSetting:
				m_db.setUserSetting!"stdin"(u.message.chat.id, u.message.from.id, "");
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"empty_stdin");
				break;
			case changeArgsSetting:
				m_db.setUserSetting!"args"(u.message.chat.id, u.message.from.id, "");
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"empty_args");
				break;
			case executeCode:
				m_bot.sendMessageForceReply(u.message.chat.id, Speech!"execute_code_cannot_empty");
				return; // Because bot still needs to wait for code
			}
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.none);
			break;
		case "/cancel":
			m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.none);
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"cancel_command");
			break;
		default:
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"unknown_command".format(u.message.text));
			break;
		}
	}

	void handlePendingAction(Update u)
	in {
		assert(!u.message.isNull);
		assert(!u.message.text.isNull);
		assert(!u.message.from.isNull);
	} body {
		"Handling pending action".logDebug;

		final switch(m_db.getUserAction(u.message.chat.id, u.message.from.id)) with(Action) {
		case none:
			"Somebody just went bad: %s".logWarn(u.serializeToJson);
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"breaking_bad");
			break;
		case changeCompilerSetting:
			import std.ascii : toLower;
			auto c = u.message.text.map!toLower;
			if(c.equal("dmd") || c.equal("ldc")) {
				m_db.setUserSetting!"compiler"(u.message.chat.id, u.message.from.id, c.to!string);
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"compiler_set".format(c));
			} else {
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"compiler_unknown");
			}
			break;
		case changeStdinSetting:
			if(u.message.text.length <= MAX_STDIN_LENGTH) {
				m_db.setUserSetting!"stdin"(u.message.chat.id, u.message.from.id, u.message.text);
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"stdin_set".format(u.message.text));
			} else {
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"stdin_limit".format(MAX_STDIN_LENGTH));
			}
			break;
		case changeArgsSetting:
			if(u.message.text.length <= MAX_ARGS_LENGTH) {
				m_db.setUserSetting!"args"(u.message.chat.id, u.message.from.id, u.message.text);
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"args_set".format(u.message.text));
			} else {
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"args_limit".format(MAX_ARGS_LENGTH));
			}
			break;
		case executeCode:
			if(u.message.text.length <= MAX_CODE_LENGTH) {
				this.executeCode(u.message);
			} else {
				m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"code_limit".format(MAX_CODE_LENGTH));
				return; // Action is still going so user doesn't have to type /execute over and over again;
			}
		}
		m_db.setUserAction(u.message.chat.id, u.message.from.id, Action.none);
	}

	void handleMessage(Update u)
	in {
		assert(!u.message.isNull);
		assert(!u.message.text.isNull);
		assert(!u.message.from.isNull);
	} body {
		"Handling message %s".logDebug(u.message.text);

		if(u.message.text.length <= MAX_CODE_LENGTH) {
			this.executeCode(u.message);
		} else {
			m_bot.sendMessageMarkdown(u.message.chat.id, Speech!"code_limit".format(MAX_CODE_LENGTH));
		}
	}

	void handleEditedMessage(Update u)
	in {
		assert(!u.edited_message.isNull);
		assert(!u.edited_message.text.isNull);
		assert(!u.edited_message.from.isNull);
	} body {
		"Handling edited message %s".logDebug(u.edited_message.text);

		if(u.edited_message.text.length <= MAX_CODE_LENGTH) {
			this.executeCode!true(u.edited_message);
		} else {
			m_bot.sendMessageMarkdown(u.edited_message.chat.id, Speech!"code_limit".format(MAX_CODE_LENGTH));
		}
	}

	void handleOtherUpdates(Update u) {
		"Handling other update".logDebug;

		Message m;
		if(!u.edited_message.isNull) {
			m = u.edited_message.get;
		} else if(!u.message.isNull) {
			m = u.message.get;
		} else {
			"Got a weird update. What can that be?: %s".logWarn(u.serializeToJson);
			return;
		}

		if(!m.audio.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_audio");
		} else if(!m.document.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_document");
		} else if(!m.game.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_game");
		} else if(m.photo.length) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_photo");
		} else if(!m.sticker.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_sticker");
		} else if(!m.video.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_video");
		} else if(!m.voice.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_voice");
		} else if(!m.video_note.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_video_note");
		} else if(!m.contact.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_contact");
		} else if(!m.location.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_location");
		} else if(!m.venue.isNull) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"other_venue");
		}
	}

	void handleCallback(Update u)
	in {
		assert(!u.callback_query.isNull);
	} body {
		"Handling callback".logDebug;

		if(u.callback_query.data.isNull || u.callback_query.message.isNull) {
			"Callback without needed fields: %s".logWarn(u.serializeToJson);
			return;
		}

		auto chat_id = u.callback_query.message.chat.id;
		auto user_id = u.callback_query.from.id;
		auto message_id = u.callback_query.message.id;

		AnswerCallbackQueryMethod m = {
			callback_query_id: u.callback_query.id,
		};

		free: switch(u.callback_query.data) {
			static foreach(reaction; ["thumbs_up", "thumbs_down"]) {
				case reaction:
					if(m_db.userAlreadyReacted!reaction(chat_id, message_id, user_id)) {
						m.text = Speech!("remove_reaction_" ~ reaction);
						m_db.removeReactionToMessage!reaction(chat_id, message_id, user_id);
					} else {
						m.text = Speech!("reaction_" ~ reaction);
						m_db.reactToMessage!reaction(chat_id, message_id, user_id);
					}
					break free;
			}
			default:
				m.text = Speech!"wrong_callback";
				m_bot.answerCallbackQuery(m);
				return;
		}

		m_bot.updateMessageButtons(m_db, chat_id, message_id);
		m_bot.answerCallbackQuery(m);
	}

	void executeCode(bool edit_old_message = false)(Nullable!Message m)
	in {
		assert(!m.isNull);
		assert(!m.text.isNull);
		assert(!m.from.isNull);
	} body {
		runTask({
			m_bot.sendChatAction(m.chat.id, "typing");
		});

		auto result = m_run.run(m.text, m_db.getUserSettings(m.chat.id, m.from.id));

		if(!result.success) {
			m_bot.sendMessageMarkdown(m.chat.id, m.id, Speech!"execute_code_backend_error".format(result.output));
			m_db.setUserAction(m.chat.id, m.from.id, Action.none);
			return;
		}

		runTask({
			m_db.addStatistics(result.errors.length, result.warnings.length, m.text.length, result.output.length);
		});

		import std.array : Appender;
		Appender!string msg;

		if(result.errors.length) {
			msg.formattedWrite(Speech!"execute_code_error");
		} else if(result.warnings.length) {
			msg.formattedWrite(Speech!"execute_code_warning");
		} else {
			msg.formattedWrite(Speech!"execute_code_success");
		}

		if(result.output.length)
			msg.formattedWrite!"\nOutput:\n```\n%s\n```"(result.output.take(MAX_OUTPUT_LENGTH));

		if(result.output.length > MAX_OUTPUT_LENGTH)
			msg.formattedWrite!"\n%s"(Speech!"execute_code_output_truncated");

		if(result.errors.length || result.warnings.length) {
			import std.range : enumerate, chain;
			import std.algorithm : filter, canFind;
			import std.string : lineSplitter;

			msg.formattedWrite!"\n%s"(Speech!"execute_code_showing_error");
			msg.put("\n```");
			m.text
				.lineSplitter
				.enumerate(1)
				.filter!(a => result.warnings
									.chain(result.errors)
									.canFind!(b =>  b.line     == a.index ||
												b.line + 1 == a.index ||
												b.line - 1 == a.index))
				.each!((a) {
					msg.formattedWrite!"\n%3d | %s"(a.index, a.value);
					result.warnings
						.chain(result.errors)
						.filter!(b => b.line == a.index)
						.each!(b => msg.formattedWrite!"\n^^^ | %s"(b.message));
				});
			msg.put("\n```");
		}

		Message reply;

		static if(edit_old_message) {
			auto reply_id = m_db.getReplyId(m.chat.id, m.from.id);

			if(reply_id) {
				auto r = m_bot.editMessageTextWithButtons(m_db, m.chat.id, reply_id, msg.data);

				if(r.type == typeid(Message)) {
					return;
				} else {
					reply = m_bot.sendMessageWithButtons(m.chat, m.id, msg.data);
				}
			} else {
				reply = m_bot.sendMessageWithButtons(m.chat, m.id, msg.data);
			}
		} else {
			reply = m_bot.sendMessageWithButtons(m.chat, m.id, msg.data);
		}

		m_db.setReplyId(m.chat.id, m.from.id, reply.id);
	}

	bool isCommand(Nullable!Message m) {
		return !m.isNull && !m.from.isNull && !m.text.isNull && m.text.startsWith("/") && !m.text.startsWith("//");
	} 

	bool hasPendingAction(Nullable!Message m) {
		return !m.isNull && !m.from.isNull && !m.text.isNull && m_db.getUserAction(m.chat.id, m.from.id) != Action.init;
	}
}

auto sendMessageMarkdown(ARGS...)(ref TelegramBot bot, long chat_id, ARGS args)
if(ARGS.length == 1 || ARGS.length == 2) {
	SendMessageMethod m = {
		chat_id: chat_id,
		parse_mode: ParseMode.markdown,
	};

	static if(is(ARGS[0] : int) && ARGS.length == 2) {
		m.reply_to_message_id = args[0];
		m.text = args[1];
	} else {
		m.text = args[0];
	}

	bot.sendMessage(m);
}

auto sendMessageForceReply(ref TelegramBot bot, long chat_id, string text) {
	ForceReply force_reply = {
		force_reply: true,
	};
	ReplyMarkup reply_markup = force_reply;
	SendMessageMethod m = {
		chat_id: chat_id,
		text: text,
		parse_mode: ParseMode.markdown,
		reply_markup: force_reply,
	};
	return bot.sendMessage(m);
}

auto sendMessageWithButtons(ref TelegramBot bot, Chat chat, int reply_to, string text) {
	InlineKeyboardButton button_thumbs_up = {
		text: "👍",
		callback_data: "thumbs_up",
	};
	InlineKeyboardButton button_thumbs_down = {
		text: "👎",
		callback_data: "thumbs_down",
	};

	SendMessageMethod m = {
		chat_id: chat.id,
		text: text,
		reply_to_message_id: reply_to,
		parse_mode: ParseMode.markdown,
		disable_web_page_preview: true,
	};

	if(chat.type != ChatType.private_)
		m.reply_markup = InlineKeyboardMarkup([
			[button_thumbs_up, button_thumbs_down],
		]);

	return bot.sendMessage(m);
}

auto updateMessageButtons(ref TelegramBot bot, ref DB db, long chat_id, int message_id) {
	EditMessageReplyMarkupMethod m = {
		chat_id: chat_id,
		message_id: message_id,
		reply_markup: InlineKeyboardMarkup([db.getReactionButtons(chat_id, message_id)]),
	};
	return bot.editMessageReplyMarkup(m);
}

auto editMessageTextWithButtons(ref TelegramBot bot, ref DB db, long chat_id, int message_id, string text) {
	EditMessageTextMethod m = {
		chat_id: chat_id,
		message_id: message_id,
		parse_mode: ParseMode.markdown,
		text: text,
		reply_markup: InlineKeyboardMarkup([db.getReactionButtons(chat_id, message_id)]),
	};
	return bot.editMessageText(m);
}

InlineKeyboardButton[] getReactionButtons(ref DB db, long chat_id, int message_id) {
	auto r = db.getMessageReactions(chat_id, message_id);
	InlineKeyboardButton button_thumbs_up = {
		text: r.thumbs_up   ? "👍 %d".format(r.thumbs_up)   : "👍",
		callback_data: "thumbs_up",
	};
	InlineKeyboardButton button_thumbs_down = {
		text: r.thumbs_down ? "👎 %d".format(r.thumbs_down) : "👎",
		callback_data: "thumbs_down",
	};

	return [button_thumbs_up, button_thumbs_down];
}

// Like `switch` but cooler
template witch(alias pred, alias func, Range) {
	auto witch(Range r) {
			return witchImpl(r);
	}

	struct witchImpl {
		private {
			Range m_range;
			ElementType!Range m_front;
			bool m_empty;
		}

		this(Range r) {
			m_range = r;
			this.popFront;
		}

		auto front() { return m_front; }
		auto empty() { return m_empty; }
		void popFront() {
			if(m_range.empty) {
				m_empty = true;
				return;
			}

			if(pred(m_range.front)) {
				func(m_range.front);
				m_range.popFront;
				this.popFront;
			} else {
				m_front = m_range.front;
				m_range.popFront;
			}
		}
	}
}