module db;

import vibe.core.log;
import vibe.db.mongo.mongo;
import vibe.data.bson;

import std.algorithm;
import std.meta;
import std.traits;
import std.conv;

struct DB {
	private {
		MongoCollection m_settings;
		MongoCollection m_reactions;
		MongoCollection m_statistics;
	}

	this(string url) {
		"Opening mongo database %s".logInfo(url);
		auto client = url.connectMongoDB;

		m_settings   = client.getCollection("dlangbot.settings");
		m_reactions  = client.getCollection("dlangbot.reactions");
		m_statistics = client.getCollection("dlangbot.statistics");
	}

	Settings getUserSettings(long chat_id, int user_id) {
		auto b = m_settings.findOne(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			[
				"users.compiler": 1,
				"users.args"    : 1,
				"users.stdin"   : 1,
			]
		);

		return b.isNull ? Settings() : b.deserializeBson!Settings;
	}

	void setUserSetting(string name)(long chat_id, int user_id, string value)
	if([FieldNameTuple!Settings].canFind(name)) {
		m_settings.findAndModifyExt(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			["$set": [name: value]],
			["upsert": true]
		);
	}

	Action getUserAction(long chat_id, int user_id) {
		import std.conv : to;
		auto b = m_settings.findOne(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			["_id": 0, "action": 1]
		);
		return b.isNull ? Action.init : b["action"].opt!int.to!Action;
	}

	void setUserAction(long chat_id, int user_id, Action action) {
		m_settings.findAndModifyExt(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			["$set": ["action": action]],
			["upsert": true]
		);
	}

	int getReplyId(long chat_id, int user_id) {
		auto b = m_settings.findOne(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			["_id": 0, "reply": 1]
		);
		return b.isNull ? 0 : b["reply"].opt!int;
	}

	void setReplyId(long chat_id, int user_id, int reply_id) {
		m_settings.findAndModifyExt(
			[
				"chat_id": Bson(chat_id),
				"user_id": Bson(user_id),
			],
			["$set": ["reply": reply_id]],
			["upsert": true]
		);
	}

	bool userAlreadyReacted(string reaction)(long chat_id, int message_id, int user_id) {
		return 1 == m_reactions.count(
			[
				"chat_id"   : Bson(chat_id),
				"message_id": Bson(message_id),
				reaction    : Bson([
					"$elemMatch": Bson(["$eq": Bson(user_id)])
				])
			]
		);
	}

	void reactToMessage(string reaction)(long chat_id, int message_id, int user_id) {
		m_reactions.findAndModifyExt(
			[
				"chat_id"   : Bson(chat_id),
				"message_id": Bson(message_id),
			],
			["$addToSet": [reaction: user_id]],
			["upsert": true],
		);
	}

	void removeReactionToMessage(string reaction)(long chat_id, int message_id, int user_id) {
		m_reactions.findAndModify(
			[
				"chat_id"   : Bson(chat_id),
				"message_id": Bson(message_id),
			],
			["$pull": [reaction: user_id]]
		);
	}

	Reactions getMessageReactions(long chat_id, int message_id) {
		auto b = m_reactions.findOne(
			[
				"chat_id": Bson(chat_id),
				"message_id": Bson(message_id),
			],
			[
				"_id": Bson(false),
				"thumbs_up": Bson(true),
				"thumbs_down": Bson(true),
			]
		);

		return b.type == Bson.Type.null_
			? Reactions.init
			: Reactions(
				b["thumbs_up"].type == Bson.Type.array
					? b["thumbs_up"].length.to!int
					: 0,
				b["thumbs_down"].type == Bson.Type.array
					? b["thumbs_down"].length.to!int
					: 0,	
			);
	}

	void addChat(long chat_id) {
		m_statistics.findAndModifyExt(
			Bson.emptyObject,
			["$addToSet": ["chats": chat_id]],
			["upsert": true],
		);
	}

	void addStatistics(size_t errors, size_t warnings, size_t characters, size_t output) {
		m_statistics.findAndModify(
			Bson.emptyObject,
			[
				"$inc": [
					"successful": errors ? 0 : 1,
					"failed"    : errors ? 1 : 0,
					"characters": characters,
					"errors"    : errors,
					"warnings"  : warnings,
					"output"    : output,
				],
			]
		);
	}

	Statistics getStatistics() {
		auto r = m_statistics.aggregate([
			[
				"$project": [
					"chats"     : Bson(["$size": Bson("$chats")]),
					"_id"       : Bson(false),
					"successful": Bson(true),
					"failed"    : Bson(true),
					"characters": Bson(true),
					"errors"    : Bson(true),
					"warnings"  : Bson(true),
					"output"    : Bson(true),
				]
			]
		]);

		if(!r.length) {
			"Error while aggregating statistics".logWarn;
			return Statistics.init;
		}

		return r[0].deserializeBson!Statistics;
	}
}

enum Action {
	none,
	changeCompilerSetting,
	changeStdinSetting,
	changeArgsSetting,
	executeCode,
}

struct Settings {
@optional:
	string compiler = "dmd";
	string stdin;
	string args;
}

struct Reactions {
@optional:
	int thumbs_up;
	int thumbs_down;
}

struct Statistics {
@optional:
	size_t chats,
		   successful,
		   failed,
		   characters,
		   output,
		   errors,
		   warnings;
}